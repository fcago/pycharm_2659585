from types import FunctionType

import pandas as pd
import pandas_flavor as pf


@pf.register_dataframe_method
def column_from_row_function(df: pd.DataFrame, column_name: str, fn: FunctionType) -> pd.DataFrame:
    df[column_name] = df.apply(fn, axis=1)
    return df



