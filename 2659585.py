from janitor import *

if __name__ == '__main__':
    def bounding_area(row: pd.DataFrame) -> float:
        return row.length * row.width


    def main():
        cols_to_select = "Petal.Length  Petal.Width".split()
        new_names = "length width".split()
        name_map = dict(zip(cols_to_select, new_names))
        petals = (pd.read_csv(
            "https://sites.google.com/site/pocketecoworld/iris.csv")
                .select_columns(cols_to_select)  # code completion fails
                .rename_columns(name_map) # code completion fails
                .column_from_row_function("bound", bounding_area) # code completion fails
                )
        print(petals.head(10))


    main()
